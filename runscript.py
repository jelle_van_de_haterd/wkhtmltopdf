#!/usr/bin/env python

import os
import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from subprocess import Popen, PIPE, STDOUT


LOGGER_FORMAT = "%(asctime)-12s %(levelname)-8s %(message)s"
logging.basicConfig(format=LOGGER_FORMAT, datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)


class CreatePDFEventHandler(FileSystemEventHandler):

    def __init__(self, output_path, args):
        self.output_path = output_path
        self.args = args
        super(CreatePDFEventHandler, self).__init__()

    def on_created(self, event):
        logger.info("Received file '%s'", event.src_path)
        filepath, file_extension = os.path.splitext(event.src_path)
        filename = os.path.basename(filepath)
        output_path = os.path.join(self.output_path, filename + ".pdf")

        logger.info("Processing...")
        process = Popen(
            ['wkhtmltopdf'] + self.args + [event.src_path, output_path],
            stdout=PIPE,
            stdin=PIPE,
            stderr=STDOUT
        )
        output, error = process.communicate()
        if process.returncode != 0:
            logger.error("wkhtmltopdf failed %d %s %s", process.returncode, output, error)
        else:
            logger.info("Done, created '%s'", output_path)

        try:
            logger.info("Removing '%s'", event.src_path)
            os.remove(event.src_path)
        except OSError as e:
            logger.error(e.message)


if __name__ == "__main__":

    logger.setLevel(logging.INFO)

    path = os.path.realpath(os.getenv('DIR_TO_WATCH', "."))
    output_path = os.path.join(os.path.dirname(path), 'output')
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    logger.info("Started watching dir '%s' for files to process", path)
    event_handler = CreatePDFEventHandler(output_path, sys.argv[1:])
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
