# jellehaterd/wkhtmltopdf

This docker container lets you run wkhtmltopdf in a docker container. 
This container uses [watchdog](https://pythonhosted.org/watchdog/) to watch for files added to the `DIR_TO_WATCH`.
A dir called `output` is created next to the `DIR_TO_WATCH`. Both dirs will be put in the `BASE_DIR` 


## Building the image
This image is based on [centos:7](https://hub.docker.com/_/centos/) 

```bash
docker build .
```

## Using the image

Create the dir where watchdog is looking for files `mkdir -p data/watching` 

```bash
docker run -it --rm -v $(pwd)/data:data --name wkhtmltopdf jellehaterd/wkhtmltopdf 
```
The mounted volume can be used to send local html files and to save the output.

If you need to pass commandline args to wkhtmltopdf update the `CMD` when running the docker like so:

```bash
docker run -it --rm -v $(pwd)/data:data --name wkhtmltopdf jellehaterd/wkhtmltopdf --encoding utf-8 --load-error-handling ignore
```

The arguments will be passed to `wkhtmltopdf` on every file it receives. This can also be used to add header/footer to the output pdf.
The header/footer files can be put next to the `watching` folder and passed to `wkhtmltopdf` like so;

```bash
docker run -it --rm -v $(pwd)/data:data --name wkhtmltopdf jellehaterd/wkhtmltopdf --header-html $BASE_DIR/header.html
```

## Docker args
| Name                 | Default Value          | Description             |
|----------------------|------------------------|-------------------------|
| `WKHTMLTOPDF_VERSION`| `0.12.4`               | The version of `wkhtmltopdf` to install |
| `BASE_DIR`           | `/data`                | Default location, also the home dir for the user and the `WORKDIR` for docker |
| `DIR_TO_WATCH`       | `${BASE_DIR}/watching` | The dir where watchdog is looking for files to convert |
| `UID`                | `1000`                 | UID for the user running `wkhtmltopdf` |
| `GID`                | `1000`                 | GID for the user running `wkhtmltopdf` |
| `USERNAME`           | `python`               | Username for the user running `wkhtmltopdf` |
| `GROUPNAME`          | `${USERNAME}`          | Groupname for the user running `wkhtmltopdf` |


## Bugs and questions

The development of the container takes place on [Bitbucket](https://bitbucket.org/jelle_van_de_haterd/wkhtmltopdf/issues). If you have a
question or a bug report to file, you can report as a bitbucket issue.