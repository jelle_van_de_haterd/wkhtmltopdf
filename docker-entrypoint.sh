#!/usr/bin/env bash
set -e

chown -R ${UID}:${GID} ${BASE_DIR}
exec runuser -u ${USERNAME} -- "$@"